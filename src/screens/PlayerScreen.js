import React, { Component } from 'react';
import shaka from 'shaka-player'
import 'shaka-player/dist/controls.css'
import {TRAILER_URL,LICENSE_URL} from '../Config/config'

class PlayerScreen extends React.PureComponent {

    constructor(props){
        super(props)
        this.videoComponent = React.createRef()
        this.onErrorEvent = this.onErrorEvent.bind(this)
        this.onError = this.onError.bind(this)
    }

    onErrorEvent(event){
        this.onError(event.detail)
    }

    onError(error){ 
        console.error("Error code ",error.code, "object", error)
    }

    componentDidMount(){
        let video = this.videoComponent.current
        var player = new shaka.Player(video)
        player.configure({
            drm: {
                servers:{'com.widevine.alpha':LICENSE_URL}
            }
        })

        player.addEventListener('error', this.onErrorEvent)
        player.load(TRAILER_URL).then(function(){
            console.log("The video has been loaded");
        }).catch(this.onError)
    }

    render() {
        return (
            <video 
					height="840"
					ref={this.videoComponent}
					poster="//shaka-player-demo.appspot.com/assets/poster.jpg"
					controls autoPlay
				/>
        );
    }
}

export default PlayerScreen;