import React, { Component } from 'react';
import Carousel from '../components/Carousel/Carousel'
import ListRowView from '../components/ListRowView/ListRowView'
import {fetchCollections} from '../Redux/Collection/collection-actions'
import {connect} from 'react-redux'
import carouselSlidesData from '../data/slides'

class HomeScreen extends Component {

    componentDidMount(){
        this.props.dispatch(fetchCollections())
    }

    render() {
        if(this.props.collections.collections.length > 0){
            const {collections} = this.props.collections
            console.log(collections.length)
            return (
                <div>
                    <Carousel slides={carouselSlidesData} />
                    {collections.map((item,index) => (
                        <ListRowView key={index} data={item} />
                    ))}
                    
                </div>
            );
        }else {
            return (
                <div></div>
            )
        }
       
    }
}

const mapStateToProps = state => {
    return {
        ...state,
        collections : state.collections
    }
}


export default connect(mapStateToProps)(HomeScreen);