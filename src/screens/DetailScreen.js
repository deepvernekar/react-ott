import React, { Component } from 'react';
import { useParams } from 'react-router-dom';
import {fetchMovies} from '../Redux/Movie/movie-actions'
import {connect} from 'react-redux'
import {Link} from "react-router-dom"
import FontAwesome from 'react-fontawesome'

class DetailScreen extends Component {


    componentDidMount(){
        const id = this.props.match.params.id
        this.props.dispatch(fetchMovies(id))
    }

    render() {
        
        if(this.props.movie.loading == false){
            const data = this.props.movie.movies.data
            const {original_title} = data
            const {artwork,snapshot} = data.images
            console.log(this.props.movie.movies.data)
            console.log(artwork)
        return (
            <div className="detail">
                <div className="detail__hero" style={{backgroundImage:"url("+snapshot+")"}}>

                    <div className="detail__content">
                       
                        <Link to="/player">
                            <button className="btn"><i className="fa fa-play fa-lg"></i></button>   
                        </Link>
                        

                        <div className="detail__info">
                            <h1>{original_title}</h1>
                        </div>
                    </div>
                </div>
            </div>
        );

        }else{
            return(
                <div></div>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        ...state,
        movies : state.movies
    }
}


export default connect(mapStateToProps)(DetailScreen);