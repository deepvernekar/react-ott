import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter as Router,Switch,Route, Link} from 'react-router-dom'
import HomeScreen from './screens/HomeScreen'
import DetailScreen from './screens/DetailScreen'
import Header from './components/Header'
import PlayerScreen from './screens/PlayerScreen';
import {Provider} from 'react-redux'
import {store} from './Redux/store'


class App extends React.Component{
    render(){
        return(
            <Provider store = {store}>
            <Router>
                <Header />
                <Switch>
                    <Route exact path="/" component={HomeScreen} />
                    <Route path="/details/:id" component={DetailScreen} />
                    <Route path="/player">
                        <PlayerScreen />
                    </Route>
                </Switch>
            </Router>
            </Provider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
