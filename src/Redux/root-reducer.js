import {combineReducers} from 'redux'
import movieReducer from './Movie/movie-reducer'
import collectionReducer from "./Collection/collection-reducer";

const rootReducer = combineReducers(
    {
        movie : movieReducer,
        collections : collectionReducer,
    }
)

export default rootReducer