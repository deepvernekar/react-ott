import CollectionActionTypes from "./collection-types";
import {addItemToList} from "./collection-utils"

const INITIAL_STATE = {
    loading : false,
    collections : [],
    error : ""
}

const collectionReducer = (state = INITIAL_STATE, action) => {
    console.log(action.payload)
    const {type,payload} = action
    switch(type){
        case CollectionActionTypes.FETCH_COLLECTION_REQUEST : return {
            ...state,
            loading : true
        }

        case CollectionActionTypes.FETCH_COLLECTION_SUCCESS : return{
            ...state,
            collections  : addItemToList(state.collections,payload),
        }

        case CollectionActionTypes.FETCH_COLLECTION_ERROR : return{
            loading : false,
            collections : [],
            error : payload
        }

        default : return INITIAL_STATE
    }
}

export default collectionReducer