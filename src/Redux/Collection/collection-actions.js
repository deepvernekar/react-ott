import axios from 'axios'
import CollectionActionTypes from './collection-types'

const rowIds = [
    "populares-en-taquilla",
    "estrenos-para-toda-la-familia",
    "estrenos-imprescindibles-en-taquilla",
    "estrenos-espanoles",
    "si-te-perdiste",
    "especial-x-men",
    "nuestras-preferidas-de-la-semana"
]
    


// action for fetch movie request
export const fetchCollectionRequest = () => {
    return {
        type : CollectionActionTypes.FETCH_COLLECTION_REQUEST
    }
}

// action for movies load success
export const fetchCollectionSuccess = movies => {
    return {
        type : CollectionActionTypes.FETCH_COLLECTION_SUCCESS,
        payload : movies
    }
}

// action for movies load failure
export const fetchCollectionError = error =>{
    return {
        type : CollectionActionTypes.FETCH_COLLECTION_ERROR,
        payload : error
    }
}

// code for fetch movies and dispatchs actions
export function fetchCollections(){
    return (dispatch) => {
        //dispatch(fetchCollectionRequest)
        rowIds.map((id,index) =>(
            axios.get("https://gizmo.rakuten.tv/v3/lists/"+id+"?classification_id=5&device_identifier=web&locale=es&market_code=es")
            .then(response => {
                const movies = response.data
                dispatch(fetchCollectionSuccess(movies))
            })
            .catch(error => {
                dispatch(fetchCollectionError(error.message))
            })
        ))
       
    }
}