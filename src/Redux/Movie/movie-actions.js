import axios from 'axios'
import MovieActionTypes from './movie-types'

// action for fetch movie request
export const fetchMoviesRequest = () => {
    return {
        type : MovieActionTypes.FETCH_MOVIES_REQUEST
    }
}

// action for movies load success
export const fetchMoviesSuccess = movies => {
    return {
        type : MovieActionTypes.FETCH_MOVIES_SUCCESS,
        payload : movies
    }
}

// action for movies load failure
export const fetchMoviesFailure = error =>{
    return {
        type : MovieActionTypes.FETCH_MOVIES_FAILURE,
        payload : error
    }
}

// code for fetch movies and dispatchs actions
export function fetchMovies(id){
    return (dispatch) => {
        dispatch(fetchMoviesRequest)
        axios.get("https://gizmo.rakuten.tv/v3/movies/"+id+"?classification_id=5&device_identifier=web&locale=es&market_code=es")
        .then(response => {
            const movies = response.data
            dispatch(fetchMoviesSuccess(movies))
        })
        .catch(error => {
            dispatch(fetchMoviesFailure(error.message))
        })
    }
}