import MovieActionTypes from './movie-types'

const INITIAL_STATE = {
    loading : true,
    movies : [],
    error : ""
}

const movieReducer = (state = INITIAL_STATE,action) => {
    const {type,payload} = action
    switch(type){
        case MovieActionTypes.FETCH_MOVIES_REQUEST : return {
            ...state,
            loading : true
        }

        case MovieActionTypes.FETCH_MOVIES_SUCCESS : return {
            loading : false,
            movies  : payload,
            error : ""
        }

        case MovieActionTypes.FETCH_MOVIES_FAILURE : return {
            loading : false,
            movies : [],
            error : payload
        }
        default : return INITIAL_STATE
    }

}

export default movieReducer