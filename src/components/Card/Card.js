import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import './Card.css'

class Card extends Component {
    render() {
        const {artwork,snapshot} = this.props.data.images
        const {id} = this.props.data
        const image_resized = artwork.replace(".jpeg","-width217-quality80.jpeg")
      
        return (
            <Link to={"/details/"+id}>
            <div className="card">
                <img src = {image_resized}/>
            </div>
            </Link>
        );
    }
}

export default Card;