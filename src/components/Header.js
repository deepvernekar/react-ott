import React, { Component } from 'react';
import '../index.css'
import {Link} from "react-router-dom"

class Header extends Component {
    render() {
        return (
            <div className="header">
                <Link to="/">
                    <img src="https://waitforit.rakuten.tv/assets/R_TV-WHITE.png"/>
                </Link>
                
            <div className="header-right">
                <a href="#home">MOVIES</a>
                <a href="#contact">ENTERTAINMENT</a>
                <a href="#about">HOLLYWOOD</a>
            </div>
        </div>
        );
    }
}

export default Header;