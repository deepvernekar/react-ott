import React, { Component } from 'react';
import Card from '../Card/Card'
import './ListRowView.css'


class ListRowView extends Component {

    render() {
        const {data} = this.props.data
        const {short_name} = data
        const items = data.contents.data
        console.log(items)

        //const {data} = this.props.data
        return (
            <div className="list__row__view">
                <h3>{short_name}</h3>
                <div className="list__row__view__wrapper">
                    {items.map((item,index) => (
                         <Card key={index} data={item} />
                    ))}
                </div>  
            </div>
        );
    }
}

export default ListRowView;