import React from 'react'
import ReactDOM from 'react-dom'
import CarouselSlide from './CarouselSlide'
import CarouselIndicator from './CarouselIndicator'
import CarouselRightArrow from './CarouselRightArrow'
import CarouselLeftArrow from './CarouselLeftArrow'

class Carousel extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            activeIndex : 0
        }
    }

    gotoSlide(index){
        this.setState({
            activeIndex : index
        })
    }

    goToPrevSlide(e){
        e.preventDefault();
        let index = this.state.activeIndex;
        let {slides} = this.props
        let slidesLength = slides.length;

        if(index < 1){
            index = slidesLength
        }
        --index;
        this.setState({
            activeIndex : index
        })
        console.log(index)
    }

    goToNextSlide(e){
        e.preventDefault();
        let index = this.state.activeIndex;
        let {slides} = this.props
        let slidesLength = slides.length;

        if(index == slidesLength-1){
            index = -1;
        }

        ++index

        this.setState({
            activeIndex : index
        });

        console.log(index)
    }

    render(){
        return(
            <div className="carousel">
            
            <ul className="carousel__slides">
                {this.props.slides.map((slide,index) => 
                    <CarouselSlide 
                        key={index} 
                        index={index} 
                        activeIndex={this.state.activeIndex} 
                        slide={slide}
                    />
                )}
            </ul>

            <CarouselLeftArrow onClick={e => this.goToPrevSlide(e)} />
            <CarouselRightArrow onClick={e => this.goToNextSlide(e)} />

            <ul className="carousel__indicators">
                {this.props.slides.map((slide,index) => 
                    <CarouselIndicator 
                        key = {index}
                        index = {index}
                        activeIndex={this.state.activeIndex}
                        isActive={this.state.activeIndex==index}
                        onClick = {e => this.gotoSlide(index)}
                    />
                )}
            </ul>

        </div>
        )
    }
}

export default Carousel
