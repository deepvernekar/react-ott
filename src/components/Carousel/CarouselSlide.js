import React from 'react'
import ReactDOM from 'react-dom'

class CarouselSlide extends React.Component{
    render(){
        console.log(this.props.slide.image)
        return(
            <li className={
                this.props.index == this.props.activeIndex 
                ?"carousel__slide carousel__slide--active"
                :"carousel__slide"}
                
                style={{backgroundImage:"url("+this.props.slide.image+")"}}
                >
                

            </li>
        )
    }
}

export default CarouselSlide