const API_URL = "localhost.rakuten.tv:3000"
const TRAILER_URL = 'https://storage.googleapis.com/shaka-demo-assets/sintel-widevine/dash.mpd'
const LICENSE_URL = 'https://cwip-shaka-proxy.appspot.com/no_auth'

export {
    API_URL,
    TRAILER_URL,
    LICENSE_URL
}